# Inkscape Cookbook for Scientists

The goal is to populate this repository with recipes for:

1. Making figures for scientific manuscripts:
    * Organise panels: align horizontally and vertically.
    * Crop imported images using a clip rectangle, making all panels the same size.
	* Copy size; paste width or height to make items match in dimensions.
    * Add layers with lettering.
    * Group panels and their letters, and scale them with/without scaling letters.

2. Making posters for printing, for scientific conferences:
    * Create a template with title, authors, and multiple sections.
    * Explain how to set the dimensions and make them match those of the printer.
    * Suggestions on how to organise text in readable columns that aren't too wide.

3. Scripting:
    * Plotting data.
    * Generating images.

4. PDFs:
    * Extracting images from papers.
    * Importing papers with the fonts intact, by installing the necessary system fonts.

5. Presentation slides:
    * Handling multi-page documents.

